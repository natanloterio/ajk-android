# ajk-android

## Passos para deploy ##

## Qualidade de código

This project integrates a combination of unit tests, functional test and code analysis tools. 
Este projeto integra uma combinaçlão de testes unitários, funcionais e outras análise de código

### Testes

Para executar os testes unitários, rode:

``` 
./gradlew test
``` 

Para executar os testes funcionais em aparelhos conectados:

``` 
./gradlew connectedAndroidTest
``` 

### Ferramenta que ajuda a encontrar bugs

```
./gradlew findbugs
```

### Checkstyle do código

```
./gradlew checkstyle
```

 
## Distribuição do app

Esse app pode ser distribuido usando tanto o [Crashlytics](http://support.crashlytics.com/knowledgebase/articles/388925-beta-distributions-with-gradle) ou a [Google Play Store](https://github.com/Triple-T/gradle-play-publisher).


### Play Store

Para distribuir o app via __Gradle Play Publisher__ você precisará instalar o plugin no seu Android Studio. 

```
./gradlew publishApkRelease
```
Para tirar dúvidas leia [documentação do plugin](https://github.com/Triple-T/gradle-play-publisher) 

### Crashlytics

Você também pode distribuir o app via o Crashlytics. Será necessário informar os dados da conta
no arquivo:`app/src/fabric.properties`.

Para fazer deploy no Crashlytics basta rodar:

```
./gradlew assembleRelease crashlyticsUploadDistributionRelease
```
