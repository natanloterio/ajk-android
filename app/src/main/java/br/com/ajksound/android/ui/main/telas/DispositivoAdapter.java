package br.com.ajksound.android.ui.main.telas;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.ajksound.android.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by fabriciooliveira on 5/6/18.
 */

public class DispositivoAdapter extends RecyclerView.Adapter<DispositivoAdapter.CustomViewHolder> {

    private List<String> mDispositivos;

    @Inject
    public DispositivoAdapter() {
        mDispositivos = new ArrayList<>();
    }

    public void setDispositivos(List<String> dispositivos) {
        mDispositivos = dispositivos;
    }

    @Override
    public DispositivoAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dispositivo, parent, false);
        return new DispositivoAdapter.CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DispositivoAdapter.CustomViewHolder holder, int position) {
        String currentDispositivo = mDispositivos.get(position);

        holder.nameTextView.setText(String.format("%s", currentDispositivo));
    }

    @Override
    public int getItemCount() {
        return mDispositivos.size();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.device)
        TextView nameTextView;


        public CustomViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
