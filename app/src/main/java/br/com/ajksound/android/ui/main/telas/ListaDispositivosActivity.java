package br.com.ajksound.android.ui.main.telas;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.ajksound.android.R;
import br.com.ajksound.android.ui.base.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by fabriciooliveira on 5/6/18.
 */

public class ListaDispositivosActivity extends BaseActivity {

    @BindView(R.id.toolbar_devices_layout)
    Toolbar mToolbar;

    @BindView(R.id.toolbar_title)
    TextView mTitle;

    @BindView(R.id.top_recycler_view)
    RecyclerView mTopRecyclerView;

    @BindView(R.id.bottom_recycler_view)
    RecyclerView mBottomRecyclerView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_dispositivos);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mToolbar.setTitle("");
        mToolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);
        mTitle.setText(getString(R.string.toolbar_title_devices));

        mockTopRecyclerView();
        mockBottomRecyclerView();
    }

    private void mockTopRecyclerView(){
        mTopRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        DispositivoAdapter adapter = new DispositivoAdapter();
        adapter.setDispositivos(mockListDevices());

        mTopRecyclerView.setAdapter(adapter);

    }

    private void mockBottomRecyclerView(){
        mBottomRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        DispositivoAdapter adapter = new DispositivoAdapter();
        adapter.setDispositivos(mockListDevices());

        mBottomRecyclerView.setAdapter(adapter);

    }

    private List<String> mockListDevices() {
        List<String> lista = new ArrayList<>();
        lista.add("Moto G4");
        lista.add("LG G2");
        lista.add("Samsung Galaxy s5");
        lista.add("LG K4");
        lista.add("LG K2");
        lista.add("Sony Xperia");
        lista.add("Moto G4");
        lista.add("Moto G4");
        lista.add("Moto G4");

        return lista;
    }
}
