package br.com.ajksound.android.injection.component;

import dagger.Subcomponent;
import br.com.ajksound.android.injection.PerActivity;
import br.com.ajksound.android.injection.module.ActivityModule;
import br.com.ajksound.android.ui.main.MainActivity;

/**
 * This component inject dependencies to all Activities across the application
 */
@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity mainActivity);

}
