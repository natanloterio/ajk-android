package br.com.ajksound.android.data.model;

import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

@AutoValue
public abstract class Cena implements Comparable<Cena>, Parcelable {

    public abstract Profile profile();

    public static Cena create(Profile profile) {
        return new AutoValue_Cena(profile);
    }

    public static TypeAdapter<Cena> typeAdapter(Gson gson) {
        return new AutoValue_Cena.GsonTypeAdapter(gson);
    }

    @Override
    public int compareTo(@NonNull Cena another) {
        return profile().name().first().compareToIgnoreCase(another.profile().name().first());
    }
}

