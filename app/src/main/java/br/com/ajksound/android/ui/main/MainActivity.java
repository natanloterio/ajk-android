package br.com.ajksound.android.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import br.com.ajksound.android.data.model.Cena;
import br.com.ajksound.android.ui.main.telas.DispositivoDialogFragment;
import br.com.ajksound.android.ui.main.telas.ListaDispositivosActivity;
import br.com.ajksound.android.ui.main.telas.NovoEfeitoActivity;
import br.com.ajksound.android.util.BottomNavigationHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import br.com.ajksound.android.R;
import br.com.ajksound.android.data.SyncService;
import br.com.ajksound.android.ui.base.BaseActivity;
import br.com.ajksound.android.util.DialogFactory;

public class MainActivity extends BaseActivity implements MainMvpView {

    private static final String EXTRA_TRIGGER_SYNC_FLAG =
            "uk.co.ribot.androidboilerplate.ui.main.MainActivity.EXTRA_TRIGGER_SYNC_FLAG";

    @Inject MainPresenter mMainPresenter;
    @Inject
    CenasAdapter mCenasAdapter;

    @BindView(R.id.recycler_view) RecyclerView mRecyclerView;

    @BindView(R.id.toolbar_layout) Toolbar mToolbar;

    @BindView(R.id.toolbar_title) TextView mTitle;

    @BindView(R.id.bottom_navigation) BottomNavigationView mBottomNavigationView;

    @BindView(R.id.fab) FloatingActionButton mFAB;

    /**
     * Return an Intent to start this Activity.
     * triggerDataSyncOnCreate allows disabling the background sync service onCreate. Should
     * only be set to false during testing.
     */
    public static Intent getStartIntent(Context context, boolean triggerDataSyncOnCreate) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(EXTRA_TRIGGER_SYNC_FLAG, triggerDataSyncOnCreate);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mToolbar.setTitle("");
        mTitle.setText(getString(R.string.toolbar_title_new_scene));

        mRecyclerView.setAdapter(mCenasAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mMainPresenter.attachView(this);
        mMainPresenter.loadRibots();

        if (getIntent().getBooleanExtra(EXTRA_TRIGGER_SYNC_FLAG, true)) {
            startService(SyncService.getStartIntent(this));
        }

        // TODO: 5/5/18 COLOCAR NO PRESENTER
        BottomNavigationHelper.disableShiftMode(mBottomNavigationView);
        mBottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    Intent intent = null;

                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_item1:
                                //selectedFragment = ItemOneFragment.newInstance();

                                intent = new Intent(MainActivity.this, ListaDispositivosActivity.class);
                                startActivity(intent);

                                break;
                            case R.id.action_item2:
                                //selectedFragment = ItemTwoFragment.newInstance();

                                chamarDialogDispositivos();

                                break;
                            case R.id.action_item3:
                                //selectedFragment = ItemThreeFragment.newInstance();
                                Toast.makeText(MainActivity.this, "Gentleman item 3", Toast.LENGTH_SHORT).show();

                                break;

                            case R.id.action_item4:
                                //selectedFragment = ItemThreeFragment.newInstance();
                                Toast.makeText(MainActivity.this, "Gentleman item 3", Toast.LENGTH_SHORT).show();

                                break;

                        }
//                        breakFragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//                        transaction.replace(R.id.frame_layout, selectedFragment);
//                        transaction.commit();
                        return true;
                    }

                });


        mFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NovoEfeitoActivity.class);
                startActivity(intent);
            }
        });
    }


    private void chamarDialogDispositivos() {
        FragmentManager fm = getSupportFragmentManager();
        DispositivoDialogFragment descargaDialogFragment = DispositivoDialogFragment.newInstance();
        descargaDialogFragment.setCancelable(false);

        descargaDialogFragment.show(fm, DispositivoDialogFragment.TAG);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        mMainPresenter.detachView();
    }

    /***** MVP View methods implementation *****/

    @Override
    public void showRibots(List<Cena> cenas) {
        mCenasAdapter.setRibots(cenas);
        mCenasAdapter.notifyDataSetChanged();
    }

    @Override
    public void showError() {
        DialogFactory.createGenericErrorDialog(this, getString(R.string.error_loading))
                .show();
    }

    @Override
    public void showRibotsEmpty() {
        mCenasAdapter.setRibots(Collections.<Cena>emptyList());
        mCenasAdapter.notifyDataSetChanged();
        Toast.makeText(this, R.string.empty_ribots, Toast.LENGTH_LONG).show();
    }

}
