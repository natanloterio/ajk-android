package br.com.ajksound.android.data.local;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.VisibleForTesting;

import com.squareup.sqlbrite2.BriteDatabase;
import com.squareup.sqlbrite2.SqlBrite;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import br.com.ajksound.android.data.model.Cena;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Scheduler;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class DatabaseHelper {

    private final BriteDatabase mDb;

    @Inject
    public DatabaseHelper(DbOpenHelper dbOpenHelper) {
        this(dbOpenHelper, Schedulers.io());
    }

    @VisibleForTesting
    public DatabaseHelper(DbOpenHelper dbOpenHelper, Scheduler scheduler) {
        SqlBrite.Builder briteBuilder = new SqlBrite.Builder();
        mDb = briteBuilder.build().wrapDatabaseHelper(dbOpenHelper, scheduler);
    }

    public BriteDatabase getBriteDb() {
        return mDb;
    }

    public Observable<Cena> setRibots(final Collection<Cena> newCenas) {
        return Observable.create(new ObservableOnSubscribe<Cena>() {
            @Override
            public void subscribe(ObservableEmitter<Cena> emitter) throws Exception {
                if (emitter.isDisposed()) return;
                BriteDatabase.Transaction transaction = mDb.newTransaction();
                try {
                    mDb.delete(Db.CenaTable.TABLE_NAME, null);
                    for (Cena cena : newCenas) {
                        long result = mDb.insert(Db.CenaTable.TABLE_NAME,
                                Db.CenaTable.toContentValues(cena.profile()),
                                SQLiteDatabase.CONFLICT_REPLACE);
                        if (result >= 0) emitter.onNext(cena);
                    }
                    transaction.markSuccessful();
                    emitter.onComplete();
                } finally {
                    transaction.end();
                }
            }
        });
    }

    public Observable<List<Cena>> getRibots() {
        return mDb.createQuery(Db.CenaTable.TABLE_NAME,
                "SELECT * FROM " + Db.CenaTable.TABLE_NAME)
                .mapToList(new Function<Cursor, Cena>() {
                    @Override
                    public Cena apply(@NonNull Cursor cursor) throws Exception {
                        return Cena.create(Db.CenaTable.parseCursor(cursor));
                    }
                });
    }

}
