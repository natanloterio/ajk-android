package br.com.ajksound.android.ui.main.telas;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.anton46.stepsview.StepsView;

import br.com.ajksound.android.R;
import br.com.ajksound.android.ui.base.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by fabriciooliveira on 5/5/18.
 */

public class NovoEfeitoActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.toolbar_effect_layout)
    Toolbar mToolbar;

    @BindView(R.id.toolbar_title)
    TextView mTitle;

    @BindView(R.id.button_static)
    AppCompatButton mButtonStaticMode;

    @BindView(R.id.button_address)
    AppCompatButton mButtonAddressMode;

    @BindView(R.id.button_musical)
    AppCompatButton mButtonMusicalMode;

    @BindView(R.id.button_matrix)
    AppCompatButton mButtonMatrixMode;

    @BindView(R.id.stepsView)
    StepsView mStepsView;

    String[] stepLabels = {"", "", ""};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo_efeito);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setTitle("");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mToolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);


        mTitle.setText(getString(R.string.toolbar_title_new_effect));

        mButtonStaticMode.setOnClickListener(this);
        mButtonAddressMode.setOnClickListener(this);
        mButtonMusicalMode.setOnClickListener(this);
        mButtonMatrixMode.setOnClickListener(this);

        mStepsView.setLabels(stepLabels)
                .setBarColorIndicator(getResources().getColor(R.color.light_gray))
                .setProgressColorIndicator(getResources().getColor(R.color.red))
                .setLabelColorIndicator(getResources().getColor(R.color.red))
                .setCompletedPosition(0)
                .drawView();

    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.button_static:
                intent = new Intent(NovoEfeitoActivity.this, ModoEstaticoActivity.class);
                startActivity(intent);
                break;
            case R.id.button_address:
                intent = new Intent(NovoEfeitoActivity.this, ModoEnderecavelActivity.class);
                startActivity(intent);
                break;
            case R.id.button_musical:
                intent = new Intent(NovoEfeitoActivity.this, ModoMusicalActivity.class);
                startActivity(intent);

                break;
            case R.id.button_matrix:

                break;
        }
    }
}
