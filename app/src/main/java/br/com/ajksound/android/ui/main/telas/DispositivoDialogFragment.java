package br.com.ajksound.android.ui.main.telas;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import br.com.ajksound.android.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by fabriciooliveira on 5/6/18.
 */

public class DispositivoDialogFragment extends DialogFragment implements DialogInterface.OnDismissListener {

    public static final String TAG = DispositivoDialogFragment.class.getSimpleName();

    //@BindView(R.id.device_confirm)
    TextView mTextViewConfirm;

    //@BindView(R.id.device_cancel)
    TextView mTextViewCancel;

    public static DispositivoDialogFragment newInstance() {
        return new DispositivoDialogFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return inflater.inflate(R.layout.dispositivo_dialog, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ButterKnife.bind(getActivity());

        final View view = getView();
        if (view != null) {


            mTextViewConfirm = (TextView) view.findViewById(R.id.device_confirm);
            mTextViewConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            mTextViewCancel = (TextView) view.findViewById(R.id.device_cancel);
            mTextViewCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
        }
    }


}
