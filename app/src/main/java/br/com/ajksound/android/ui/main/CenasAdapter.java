package br.com.ajksound.android.ui.main;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import br.com.ajksound.android.R;
import br.com.ajksound.android.data.model.Cena;

public class CenasAdapter extends RecyclerView.Adapter<CenasAdapter.CustomViewHolder> {

    private List<Cena> mCenas;

    @Inject
    public CenasAdapter() {
        mCenas = new ArrayList<>();
    }

    public void setRibots(List<Cena> cenas) {
        mCenas = cenas;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cena, parent, false);
        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, int position) {
        Cena cena = mCenas.get(position);
        holder.hexColorView.setBackgroundColor(Color.parseColor(cena.profile().hexColor()));
        holder.nameTextView.setText(String.format("%s %s",
                cena.profile().name().first(), cena.profile().name().last()));
        holder.emailTextView.setText(cena.profile().email());
    }

    @Override
    public int getItemCount() {
        return mCenas.size();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.view_hex_color) View hexColorView;
        @BindView(R.id.text_name) TextView nameTextView;
        @BindView(R.id.text_email) TextView emailTextView;

        public CustomViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
