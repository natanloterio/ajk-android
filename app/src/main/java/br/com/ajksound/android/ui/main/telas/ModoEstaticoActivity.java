package br.com.ajksound.android.ui.main.telas;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.anton46.stepsview.StepsView;

import br.com.ajksound.android.R;
import br.com.ajksound.android.ui.base.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by fabriciooliveira on 5/6/18.
 */

public class ModoEstaticoActivity extends BaseActivity {

    @BindView(R.id.toolbar_static_layout)
    Toolbar mToolbar;

    @BindView(R.id.toolbar_title)
    TextView mTitle;

    @BindView(R.id.stepsView)
    StepsView mStepsView;

    String[] stepLabels = {"", "", ""};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modo_estatico);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setTitle("");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mToolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);



        mTitle.setText(getString(R.string.title_static_mode));

        mStepsView.setLabels(stepLabels)
                .setBarColorIndicator(getResources().getColor(R.color.light_gray))
                .setProgressColorIndicator(getResources().getColor(R.color.red))
                .setLabelColorIndicator(getResources().getColor(R.color.red))
                .setCompletedPosition(0)
                .drawView();


    }
}
