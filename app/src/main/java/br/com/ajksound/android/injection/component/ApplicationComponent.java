package br.com.ajksound.android.injection.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import br.com.ajksound.android.data.DataManager;
import br.com.ajksound.android.data.SyncService;
import br.com.ajksound.android.data.local.DatabaseHelper;
import br.com.ajksound.android.data.local.PreferencesHelper;
import br.com.ajksound.android.data.remote.RibotsService;
import br.com.ajksound.android.injection.ApplicationContext;
import br.com.ajksound.android.injection.module.ApplicationModule;
import br.com.ajksound.android.util.RxEventBus;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(SyncService syncService);

    @ApplicationContext Context context();
    Application application();
    RibotsService ribotsService();
    PreferencesHelper preferencesHelper();
    DatabaseHelper databaseHelper();
    DataManager dataManager();
    RxEventBus eventBus();

}
