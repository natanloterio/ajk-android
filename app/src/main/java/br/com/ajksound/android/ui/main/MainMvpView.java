package br.com.ajksound.android.ui.main;

import java.util.List;

import br.com.ajksound.android.data.model.Cena;
import br.com.ajksound.android.ui.base.MvpView;

public interface MainMvpView extends MvpView {

    void showRibots(List<Cena> cenas);

    void showRibotsEmpty();

    void showError();

}
