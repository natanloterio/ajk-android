package br.com.ajksound.android.data;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import br.com.ajksound.android.data.model.Cena;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import br.com.ajksound.android.data.local.DatabaseHelper;
import br.com.ajksound.android.data.local.PreferencesHelper;
import br.com.ajksound.android.data.remote.RibotsService;

@Singleton
public class DataManager {

    private final RibotsService mRibotsService;
    private final DatabaseHelper mDatabaseHelper;
    private final PreferencesHelper mPreferencesHelper;

    @Inject
    public DataManager(RibotsService ribotsService, PreferencesHelper preferencesHelper,
                       DatabaseHelper databaseHelper) {
        mRibotsService = ribotsService;
        mPreferencesHelper = preferencesHelper;
        mDatabaseHelper = databaseHelper;
    }

    public PreferencesHelper getPreferencesHelper() {
        return mPreferencesHelper;
    }

    public Observable<Cena> syncRibots() {
        return mRibotsService.getRibots()
                .concatMap(new Function<List<Cena>, ObservableSource<? extends Cena>>() {
                    @Override
                    public ObservableSource<? extends Cena> apply(@NonNull List<Cena> cenas)
                            throws Exception {
                        return mDatabaseHelper.setRibots(cenas);
                    }
                });
    }

    public Observable<List<Cena>> getRibots() {
        return mDatabaseHelper.getRibots().distinct();
    }

}
