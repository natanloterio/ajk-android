package br.com.ajksound.android;

import android.database.Cursor;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.Arrays;
import java.util.List;

import br.com.ajksound.android.data.model.Cena;
import io.reactivex.observers.TestObserver;
import br.com.ajksound.android.data.local.DatabaseHelper;
import br.com.ajksound.android.data.local.Db;
import br.com.ajksound.android.data.local.DbOpenHelper;
import br.com.ajksound.android.test.common.TestDataFactory;
import br.com.ajksound.android.util.DefaultConfig;
import br.com.ajksound.android.util.RxSchedulersOverrideRule;

import static junit.framework.Assert.assertEquals;

/**
 * Unit tests integration with a SQLite Database using Robolectric
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = DefaultConfig.EMULATE_SDK)
public class DatabaseHelperTest {

    @Rule
    public final RxSchedulersOverrideRule mOverrideSchedulersRule = new RxSchedulersOverrideRule();

    private DatabaseHelper mDatabaseHelper;

    @Before
    public void setup() {
        if (mDatabaseHelper == null)
            mDatabaseHelper = new DatabaseHelper(new DbOpenHelper(RuntimeEnvironment.application),
                    mOverrideSchedulersRule.getScheduler());
    }

    @Test
    public void setRibots() {
        Cena cena1 = TestDataFactory.makeRibot("r1");
        Cena cena2 = TestDataFactory.makeRibot("r2");
        List<Cena> cenas = Arrays.asList(cena1, cena2);

        TestObserver<Cena> result = new TestObserver<>();
        mDatabaseHelper.setRibots(cenas).subscribe(result);
        result.assertNoErrors();
        result.assertValueSequence(cenas);

        Cursor cursor = mDatabaseHelper.getBriteDb()
                .query("SELECT * FROM " + Db.CenaTable.TABLE_NAME);
        assertEquals(2, cursor.getCount());
        for (Cena cena : cenas) {
            cursor.moveToNext();
            assertEquals(cena.profile(), Db.CenaTable.parseCursor(cursor));
        }
    }

    @Test
    public void getRibots() {
        Cena cena1 = TestDataFactory.makeRibot("r1");
        Cena cena2 = TestDataFactory.makeRibot("r2");
        List<Cena> cenas = Arrays.asList(cena1, cena2);

        mDatabaseHelper.setRibots(cenas).subscribe();

        TestObserver<List<Cena>> result = new TestObserver<>();
        mDatabaseHelper.getRibots().subscribe(result);
        result.assertNoErrors();
        result.assertValue(cenas);
    }

}
