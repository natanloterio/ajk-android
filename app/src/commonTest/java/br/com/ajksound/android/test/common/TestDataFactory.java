package br.com.ajksound.android.test.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import br.com.ajksound.android.data.model.Cena;
import br.com.ajksound.android.data.model.Name;
import br.com.ajksound.android.data.model.Profile;

/**
 * Factory class that makes instances of data models with random field values.
 * The aim of this class is to help setting up test fixtures.
 */
public class TestDataFactory {

    public static String randomUuid() {
        return UUID.randomUUID().toString();
    }

    public static Cena makeRibot(String uniqueSuffix) {
        return Cena.create(makeProfile(uniqueSuffix));
    }

    public static List<Cena> makeListRibots(int number) {
        List<Cena> cenas = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            cenas.add(makeRibot(String.valueOf(i)));
        }
        return cenas;
    }

    public static Profile makeProfile(String uniqueSuffix) {
        return Profile.builder()
                .setName(makeName(uniqueSuffix))
                .setEmail("email" + uniqueSuffix + "@ribot.co.uk")
                .setDateOfBirth(new Date())
                .setHexColor("#0066FF")
                .setAvatar("http://api.ribot.io/images/" + uniqueSuffix)
                .setBio(randomUuid())
                .build();
    }

    public static Name makeName(String uniqueSuffix) {
        return Name.create("Name-" + uniqueSuffix, "Surname-" + uniqueSuffix);
    }

}