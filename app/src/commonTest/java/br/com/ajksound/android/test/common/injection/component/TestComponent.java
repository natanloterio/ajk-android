package br.com.ajksound.android.test.common.injection.component;

import javax.inject.Singleton;

import dagger.Component;
import br.com.ajksound.android.injection.component.ApplicationComponent;
import br.com.ajksound.android.test.common.injection.module.ApplicationTestModule;

@Singleton
@Component(modules = ApplicationTestModule.class)
public interface TestComponent extends ApplicationComponent {

}
